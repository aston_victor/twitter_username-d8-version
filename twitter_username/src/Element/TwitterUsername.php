<?php

namespace Drupal\twitter_username\Element;

use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\Textfield;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a twitter_username form element.
 *
 * @FormElement("twitter_username")
 */
class TwitterUsername extends Textfield {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#maxlength'] = TWITTER_USERNAME_MAX_LENGTH;
    $info['#process'][] = [get_class($this), 'processTwitterUsername'];

    return $info;
  }

  /**
   * Process a twitter_username form element.
   */
  public static function processTwitterUsername(&$element, FormStateInterface $form_state, &$complete_form) {
    $element['#field_prefix'] = '@';

    return $element;
  }

}
