<?php

namespace Drupal\twitter_username\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'twitter_username_default' formatter.
 *
 * @FieldFormatter(
 *   id = "twitter_username_default",
 *   label = @Translation("Twitter username, as plain text"),
 *   field_types = {
 *     "twitter_username"
 *   }
 * )
 */
class TwitterUsernameDefault extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $source = [
        '#theme' => 'twitter_username_formatter_twitter_username_default',
        '#element' => $item->twitter_username,
      ];

      $elements[$delta] = [
        '#markup' => \Drupal::service('renderer')->render($source),
      ];
    }

    return $elements;
  }

}
