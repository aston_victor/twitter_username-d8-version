<?php

namespace Drupal\twitter_username\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Plugin implementation of the 'twitter_username_follow_button' formatter.
 *
 * @FieldFormatter(
 *   id = "twitter_username_follow_button",
 *   label = @Translation("Twitter username, as Follow Button"),
 *   field_types = {
 *     "twitter_username"
 *   }
 * )
 */
class TwitterUsernameFollowButton extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'data_width' => '100px',
      'data_show_count' => 'false',
      'data_show_screen_name' => 'true',
      'data_align' => 'left',
      'data_size' => 'medium',
      'data_dnt' => 'false',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['data_width'] = [
      '#title' => $this->t('Width'),
      '#type' => 'textfield',
      '#description' => $this->t('You can specify the width of the Follow Button by setting a value here (in <em>pixels</em> "100px" or <em>percentage</em> "75%").'),
      '#default_value' => $this->getSetting('data_width'),
      '#empty_option' => '',
      '#size' => 25,
    ];

    $element['data_show_count'] = [
      '#title' => $this->t('Followers count display'),
      '#type' => 'select',
      '#options' => [
        'false' => $this->t('No'),
        'true' => $this->t('Yes'),
      ],
      '#description' => $this->t('By default, the User\'s followers count is not displayed with the Follow Button . You can enable this feature here by changing this to < em>Yes </em >.'),
      '#default_value' => $this->getSetting('data_show_count'),
    ];

    $element['data_show_screen_name'] = [
      '#title' => $this->t('Show Screen Name'),
      '#type' => 'select',
      '#options' => [
        'false' => $this->t('No'),
        'true' => $this->t('Yes'),
      ],
      '#description' => $this->t('The user\'s screen name shows up by default, but you can opt not to show the screen name in the button . You can hide the screen name by changing this to < em>No </em >'),
      '#default_value' => $this->getSetting('data_show_screen_name'),
    ];

    $element['data_align'] = [
      '#title' => $this->t('Alignment'),
      '#type' => 'select',
      '#options' => [
        'left' => $this->t('Left'),
        'right' => $this->t('Right'),
      ],
      '#description' => $this->t('You can specify the alignment of the Follow Button setting this to <em>left</em> or <em>right</em>.'),
      '#default_value' => $this->getSetting('data_align'),
    ];

    $element['data_size'] = [
      '#title' => $this->t('Button Size'),
      '#type' => 'select',
      '#options' => [
        'medium' => $this->t('Medium'),
        'large' => $this->t('Large'),
      ],
      '#description' => $this->t('The size of the button can render in either <em>medium</em>, which is the default size, or in <em>large</em>.'),
      '#default_value' => $this->getSetting('data_size'),
    ];

    $element['data_dnt'] = [
      '#title' => $this->t('Opt-out of tailoring Twitter'),
      '#type' => 'select',
      '#options' => [
        'false' => $this->t('No'),
        'true' => $this->t('Yes'),
      ],
      '#description' => $this->t('Twitter buttons on your site can help us tailor content and suggestions for Twitter users. If you want to opt-out of this feature, set the optional data-dnt parameter to be true. <a href="https://support.twitter.com/articles/20169421">Learn more about tailoring Twitter</a>.'),
      '#default_value' => $this->getSetting('data_dnt'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $summary = [];

    $summary[] = $this->t('Show Followers Count: @show_count', ['@show_count' => $settings['data_show_count']]);
    $summary[] = $this->t('Show Screen Name: @show_screen_name', ['@show_screen_name' => $settings['data_show_screen_name']]);
    $summary[] = $this->t('Opt-out of tailoring Twitter: @dnt', ['@dnt' => $settings['data_dnt']]);
    $summary[] = $this->t('Width & Alignment: @width, @align', ['@width' => $settings['data_width'], '@align' => $settings['data_align']]);
    $summary[] = $this->t('Size: @size', ['@size' => $settings['data_size']]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $settings = $this->getSettings();

    foreach ($items as $delta => $item) {
      $url = Url::fromUri("http://twitter.com/@{$item->twitter_username}");
      $link = Link::fromTextAndUrl(t('Guidelines'), $url)->toRenderable();
      $link['#attached'] = ['library' => ['twitter_username/twitter_username_global']];
      $link['#attributes'] = [
        'class' => ['twitter-follow-button'],
        'data-lang' => $item->getLangcode(),
        'data-show-count' => $settings['data_show_count'],
        'data-show-screen-name' => $settings['data_show_screen_name'],
        'data-dnt' => $settings['data_dnt'],
        'data-width' => $settings['data_width'],
        'data-align' => $settings['data_align'],
        'data-size' => $settings['data_size'],
      ];

      $elements[$delta] = [
        '#markup' => \Drupal::service('renderer')->render($link),
      ];
    }

    return $elements;
  }

}
