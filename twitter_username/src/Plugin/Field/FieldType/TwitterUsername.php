<?php

namespace Drupal\twitter_username\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'twitter_username' field type.
 *
 * @FieldType(
 *   id = "twitter_username",
 *   label = @Translation("Twitter username"),
 *   description = @Translation("Store a twitter username, and provide formatters, prefixed by @."),
 *   default_widget = "twitter_username",
 *   default_formatter = "twitter_username_default"
 * )
 */
class TwitterUsername extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field) {
    return [
      'columns' => [
        'twitter_username' => [
          'type' => 'varchar',
          'length' => TWITTER_USERNAME_MAX_LENGTH,
          'not null' => FALSE,
          'sortable' => TRUE
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('twitter_username')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['twitter_username'] = DataDefinition::create('string')
      ->setLabel(t('Twitter username'));

    return $properties;
  }

}
